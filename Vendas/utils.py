from Vendas.models import TipoPeca

TOTAL_ITEM_PER_PAGE = 25


def getMenu():
    tipos_peca = [({'pai': tipo, 'filhos': TipoPeca.objects.filter(tipo_pai=tipo.id)})
                   for tipo in TipoPeca.objects.filter(tipo_pai=None)]

    return tipos_peca
