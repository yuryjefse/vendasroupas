from django.db import models
from VendasSite.settings import PATH_IMAGE_PECA


class Usuario(models.Model):
    nome = models.CharField(max_length=100)
    logradouro = models.CharField(max_length=200, blank=True)
    bairro = models.CharField(max_length=50, blank=True)
    numero = models.CharField(max_length=20, blank=True)
    telefone = models.CharField(max_length=12, blank=True)
    celular = models.CharField(max_length=12)

    def __str__(self):
        return self.nome


class TipoPeca(models.Model):
    tipo_pai = models.ForeignKey('self', blank=True, null=True)
    titulo = models.CharField(max_length=20)

    def __str__(self):
        return self.titulo


class Marca(models.Model):
    titulo = models.CharField(max_length=20)

    def __str__(self):
        return self.titulo


class Peca(models.Model):
    MASC = 'M'
    FEM = 'F'

    CHOICES = ((MASC, 'Masculino'), (FEM, 'Feminino'))

    #FKs
    tipo = models.ForeignKey(TipoPeca)
    marca = models.ForeignKey(Marca)

    #FIELDS
    sexo = models.CharField(max_length=10, choices=CHOICES, default=MASC)
    titulo = models.CharField(max_length=150)
    titulo_curto = models.CharField(max_length=50)
    quantidade = models.IntegerField(default=1)
    tamanho = models.CharField(max_length=5)
    preco = models.DecimalField(max_digits=11, decimal_places=2)
    comissao = models.DecimalField(max_digits=5, decimal_places=2)
    is_infantil = models.BooleanField("Infantil", default=False)
    is_vendido = models.BooleanField('Vendido', default=False)

    def __str__(self):
        return '{} - {} - {}'.format(self.tipo, self.marca, self.quantidade)


class ImagemPeca(models.Model):
    peca = models.ForeignKey(Peca)
    principal = models.BooleanField(default=False)
    arquivo = models.ImageField(upload_to=PATH_IMAGE_PECA)

    def __str__(self):
        return self.peca.titulo_curto


class SolicitacaoPeca(models.Model):
    usuario = models.ForeignKey(Usuario)
    data_solicitacao = models.DateTimeField('Data solicitação')

    def __str__(self):
        return '{} - {}'.format(self.usuario, self.dataSolicitacao)


class SolicitacaoPecaItem(models.Model):
    peca = models.ForeignKey(Peca)
    quantidade = models.IntegerField()
    solicitacao_peca = models.ForeignKey(SolicitacaoPeca)

    def __str__(self):
        return self.peca