import time
import os
import urllib

from django.test import LiveServerTestCase
from django.core.files import File

from Vendas.models import Peca, ImagemPeca, TipoPeca, Marca

from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.common.action_chains import ActionChains


class SearchTests(LiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        cls.selenium = WebDriver()
        super(SearchTests, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super(SearchTests, cls).tearDownClass()

    @classmethod
    def setUp(self):
        tipoPai = TipoPeca.objects.create(titulo='Blusa')
        tipoSocial = TipoPeca.objects.create(titulo='social', tipo_pai=tipoPai)
        tipoMalha = TipoPeca.objects.create(titulo='malha', tipo_pai=tipoPai)

        marcaTommy = Marca.objects.create(titulo='Tommy', caminho_arquivo='tommy.jpg')

        pecaBlusa = Peca.objects.create(titulo='Blusa colorida com detalhes de sei da onde',
                                        titulo_curto='Blusa colorida', quantidade=1, tamanho='L', preco=50.00,
                                        comissao=10.00, sexo=Peca.MASC, is_infantil=False, tipo=tipoPai, marca=marcaTommy)

        pecaMalha = Peca.objects.create(titulo='Malha colorida com detalhes de sei da onde',
                                        titulo_curto='Malha colorida', quantidade=1, tamanho='L', preco=50.00,
                                        comissao=10.00, sexo=Peca.MASC, is_infantil=False, tipo=tipoMalha,
                                        marca=marcaTommy)
        pecaSocial = Peca.objects.create(titulo='Social colorida com detalhes de sei da onde',
                                        titulo_curto='Social colorida', quantidade=1, tamanho='L', preco=50.00,
                                        comissao=10.00, sexo=Peca.MASC, is_infantil=False, tipo=tipoSocial,
                                        marca=marcaTommy)

        filePath = r'C:\Users\ysilva\Documents\Estudo\Pessoal\vendasroupas\media\pecas\corrida.jpg'
        f = open(filePath, 'r')
        fileDjango = File(f)

        pc1 = ImagemPeca()
        pc1.principal = True
        pc1.peca = pecaBlusa
        pc1.arquivo.save('corrida.jpg', fileDjango, True)

        pc1.save()
        f.close()


        #ImagemPeca.objects.create(arquivo='1.jpg', peca=pecaBlusa, principal=True)
        #ImagemPeca.objects.create(arquivo='1.jpg', peca=pecaMalha, principal=True)
        #ImagemPeca.objects.create(arquivo='1.jpg', peca=pecaSocial, principal=True)

    def test_menu_index(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/'))

        time.sleep(2)
        menuPai = self.selenium.find_element_by_link_text('BLUSAS')
        ActionChains(self.selenium).move_to_element(menuPai).perform()

        self.assertTrue(self.selenium.find_element_by_link_text('SOCIAL'))
        self.assertTrue(self.selenium.find_element_by_link_text('MALHA'))

    def test_item_tipo_pai_search(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/'))

        time.sleep(2)
        self.selenium.find_element_by_link_text('BLUSAS').click()

        self.assertTrue(self.selenium.find_element_by_link_text('Blusa colorida'))

    def test_item_tipo_filho_search(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/'))

        time.sleep(2)
        menuPai = self.selenium.find_element_by_link_text('BLUSAS')
        ActionChains(self.selenium).move_to_element(menuPai).perform()

        self.selenium.find_element_by_link_text('MALHA').click()

        time.sleep(1)

        self.assertTrue(self.selenium.find_element_by_link_text('Malha colorida'))

    def test_pagination_search(self):

        sexMas = Sexo.objects.get(id=1)

        tipoPai = TipoPeca.objects.get(id=1)

        marcaTommy = Marca.objects.get(id=1)

        for n in range(1, 27):

            pecaBlusa = Peca.objects.create(titulo='Blusa colorida com detalhes de sei da onde %s' % n,
                                            titulo_curto='Blusa colorida %s' % n, quantidade=1, tamanho='L', preco=50.00,
                                            comissao=10.00, sexo=sexMas, is_infantil=False, tipo=tipoPai, marca=marcaTommy)
            ImagemPeca.objects.create(arquivo='1.jpg', peca=pecaBlusa, principal=True)

        time.sleep(2)
        self.selenium.find_element_by_link_text('BLUSAS').click()

        pag2 = self.selenium.find_element_by_link_text('2')

        self.assertTrue(pag2)

        pag2.click()

        self.assertTrue(self.selenium.find_element_by_link_text('1'))