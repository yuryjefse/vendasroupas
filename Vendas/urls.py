from django.conf.urls import patterns, url

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',

    url(r'^$', 'Vendas.views.index.page', name='vendas_index'),
    url(r'^index$', 'Vendas.views.index.page', name='vendas_index_with_name'),
    url(r'^search/(?P<pk>\d+)$', 'Vendas.views.search.page', name='vendas_search'),
    url(r'^search/(?P<pk>\d+)/(?P<page_current>\d+)$', 'Vendas.views.search.page', name='vendas_search_pagination'),
    url(r'^search/(?P<pk>\d+)/(?P<page_current>\d+)/(?P<order_list>\d+)$', 'Vendas.views.search.page', name='vendas_search_order'),
    url(r'^main/(?P<idProduct>\d+)$', 'Vendas.views.productMain.page', name='vendas_main'),
)
