from django.shortcuts import render
from Vendas.utils import getMenu


def page(request):

    return render(request, 'Vendas/index.html', {'menu': getMenu()})