from django.shortcuts import render
from Vendas.models import Peca
from Vendas.utils import getMenu


def page(request, idProduct):

    peca = Peca.objects.get(id=idProduct)

    return render(request, 'Vendas/main.html', {'menu': getMenu(), 'peca': peca})