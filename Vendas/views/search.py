import math

from django.shortcuts import render
from Vendas.models import Peca
from django.db.models import Q
from Vendas.utils import getMenu, TOTAL_ITEM_PER_PAGE


class OrderListEnum():
    DEFAULT = 1
    PRICE_HI_LO = 2
    PRICE_LO_HI = 3


def page(request, pk, page_current=1, order_list=OrderListEnum.DEFAULT):
    page_current = int(page_current)
    order_list = int(order_list)

    start_row = (page_current - 1) * TOTAL_ITEM_PER_PAGE
    last_row = page_current * TOTAL_ITEM_PER_PAGE

    total_page = Peca.objects.filter(Q(tipo_id=pk) | Q(tipo__tipo_pai__id=pk)).count()

    total_page = math.ceil(total_page / TOTAL_ITEM_PER_PAGE)

    if page_current > total_page | page_current < 1:
        return render(request, 'vendas/search.html')

    numbers = []
    if total_page > 1:
        if page_current > 1:
            if page_current != total_page:
                numbers = [page_current - 1, page_current, page_current + 1]
            else:
                for n in range(page_current, 0, -1):
                    if numbers.__len__() != 3:
                        numbers.append(n)
                    else:
                        break
                numbers = sorted(numbers)
        else:
            for n in range(page_current, total_page + 1):
                if numbers.__len__() != 3:
                    numbers.append(n)
                else:
                    break
    else:
        numbers = [1]

    if order_list == OrderListEnum.DEFAULT:
        pecas = [({'peca': peca, 'imagem': peca.imagempeca_set.get(principal=True)})
                 for peca in Peca.objects.filter(Q(tipo_id=pk) | Q(tipo__tipo_pai__id=pk))[start_row:last_row]]
    elif order_list == OrderListEnum.PRICE_HI_LO:
        pecas = [({'peca': peca, 'imagem': peca.imagempeca_set.get(principal=True)})
                 for peca in Peca.objects.filter(Q(tipo_id=pk) | Q(tipo__tipo_pai__id=pk)).order_by('-preco')[start_row:last_row]]
    else:
        pecas = [({'peca': peca, 'imagem': peca.imagempeca_set.get(principal=True)})
                 for peca in Peca.objects.filter(Q(tipo_id=pk) | Q(tipo__tipo_pai__id=pk)).order_by('preco')[start_row:last_row]]

    return render(request, 'Vendas/search.html', {'pecas': pecas, 'menu': getMenu(),
                                                  'pagination': {'current': page_current,
                                                                 'numbers': numbers, 'pk': pk}})
