from Vendas.models import Peca, TipoPeca, Marca, SolicitacaoPeca, SolicitacaoPecaItem, Usuario, ImagemPeca
from django.contrib import admin


class MarcaAdmin(admin.ModelAdmin):
    list_display = ['titulo']

admin.site.register(Marca, MarcaAdmin)


class ImagemPecaInline(admin.TabularInline):
    model = ImagemPeca
    extra = 3


class PecasAdmin(admin.ModelAdmin):
    list_display = ['tipo', 'marca', 'quantidade', 'tamanho']
    inlines = [ImagemPecaInline]

admin.site.register(Peca, PecasAdmin)


class SolicitacaoPecaItemInline(admin.TabularInline):
    model = SolicitacaoPecaItem
    extra = 2


class SolicitacaoPecaAdmin(admin.ModelAdmin):
    list_display = ['usuario', 'data_solicitacao']
    inlines = [SolicitacaoPecaItemInline]

admin.site.register(SolicitacaoPeca, SolicitacaoPecaAdmin)


class TipoPecasAdmin(admin.ModelAdmin):
    list_display = ['titulo']

admin.site.register(TipoPeca, TipoPecasAdmin)
admin.site.register(Usuario)


